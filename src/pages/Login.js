import {Container, Form, Button} from "react-bootstrap"
import {useState, useEffect, useContext, Fragment} from "react"
import {Redirect, BrowserRouter, Switch, Route} from "react-router-dom"
// useContext is used to unpack the data from the UserContext
import UserContext from "./../UserContext.js"
import Courses from "./../pages/Courses.js"

export default function Login() {
	
	const {user, setUser} = useContext(UserContext)

	const [logEmail, setLogEmail] = useState("")
	const [logPassword, setLogPassword] = useState("")
	const [isDisabled, setIsDisabled] = useState(false)
	
	useEffect(() => {
			if(logEmail !== '' && logPassword !== '') {
				setIsDisabled(false)
			} else {
				setIsDisabled(true)
			}
	}, [logEmail, logPassword])
	
	const login = (event) => {
			event.preventDefault()
			console.log(logEmail)
			localStorage.setItem("email", logEmail)
			setUser({
				email: localStorage.getItem("email")
			})
			setLogEmail("")
			setLogPassword("")
			alert("Logged in succesfully!");
	}

	let loggedIn = (user.email !== null) ? 
		<Fragment>
			<Redirect to="courses"/>
			<BrowserRouter>
				<Switch>
					<Route exact path="/register" component={Courses}/>
				</Switch>
			</BrowserRouter>
		</Fragment>
	: 
		<Redirect to="login"/>

	

	return (
		<Fragment>
			<Container>
				<Form className="border p-3 m-4" onSubmit={(event) => login(event)}>
				  <Form.Group className="mb-3" controlId="logEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control type="email" placeholder="Enter email" value={logEmail} onChange={(event) => setLogEmail(event.target.value)}/>
				  </Form.Group>
				  <Form.Group className="mb-3" controlId="logPassword">
				    <Form.Label>Password</Form.Label>
				    <Form.Control type="password" placeholder="Password" value={logPassword} onChange={(event) => setLogPassword(event.target.value)}/>
				  </Form.Group>
				  <Button variant="success" type="submit" disabled={isDisabled}>
				    Login
				  </Button>
				</Form>
			</Container>
			{loggedIn}
		</Fragment>
	)
}