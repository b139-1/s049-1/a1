import {Redirect} from 'react-router-dom'
import {useContext, useEffect} from "react"
import UserContext from "./../UserContext.js"

export default function Logout() {
	
	const {unsetUser, setUser} = useContext(UserContext)

	unsetUser()

	useEffect(() => {
		// Set the user state back to its original value
		setUser({email: null})
	}, [])

	return (

		<Redirect to="login"/>

	)
}