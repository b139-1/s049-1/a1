import {useState, Fragment, useContext} from "react"
import {Navbar, Nav} from "react-bootstrap"
import {NavLink} from "react-router-dom"

import UserContext from './../UserContext.js'

export default function AppNavbar() {
  

  const {user} = useContext(UserContext)
  // const [user, setUser] = useState(localStorage.getItem("email"))

  //console.log(user)

  

  let leftNav = (user.email !== null) ? 
    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
  :
    <Fragment>
      <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
      <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
    </Fragment>

  return (
    <Navbar bg="primary" expand="lg">
      <Navbar.Brand as={NavLink} to="/">Course Booking</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
        </Nav>
        <Nav>
          {leftNav}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
