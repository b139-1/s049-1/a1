import {Container, Row, Col, Card, Button} from "react-bootstrap"
import {useState, useEffect} from "react"

const CourseCard = ({courseProp}) => {

	//console.log(props)
	// console.log(courseProp)

	const {name, description, price} = courseProp
	// console.log(name)
	// console.log(description)
	// console.log(price)
	// console.log(id)
	// console.log(onOffer)

	const [count, setCount] = useState(0)
	const [seats, setSeats] = useState(10)
	const [isDisabled, setIsDisabled] = useState(false)

	function enroll() {
		setCount(count + 1)
		setSeats(seats - 1)
	}

	useEffect(() => {
		if(seats === 0) {
			setIsDisabled(true)
		}
	}, [seats])

	return (
		<Container fluid className="m-4">
			<Row>
				<Col>
					<Card>
					  <Card.Body>
					  		<Card.Title>{name}</Card.Title>
					    	<Card.Text>
					    	Description:
				    		<p>{description}</p>
				    		Price:
				    		<p>Php {price}</p>
					    	</Card.Text>
					    	<Card.Text>Enrollees: {count}</Card.Text>
					    	<Card.Text>Seats Available: {seats}</Card.Text>
					    	<Button variant="primary" onClick={enroll} disabled={isDisabled}>Enroll</Button>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}

export default CourseCard