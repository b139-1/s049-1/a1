const message = [
	
	{
		title: "Zuitt Coding Bootcamp",
		message: "Opportunities for everyone, everywhere",
		option: "Enroll now!",
		destination: "/courses"
	},
	{
		title: "Page Not Found!",
		message: "Apparently, we couldn't find what you're looking for",
		option: "Homepage",
		destination: "/"
	}
	
]

export default message