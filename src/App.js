// React Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
// React modules
import {UserProvider} from "./UserContext.js"
import {useState, Fragment} from "react"
import {BrowserRouter, Switch, Route} from "react-router-dom"
import {Container} from "react-bootstrap"
// Components
import AppNavbar from "./components/AppNavbar.js"
// Pages
import Home from "./pages/Home.js"
import Courses from "./pages/Courses.js"
import Register from "./pages/Register.js"
import Login from './pages/Login.js'
import Logout from "./pages/Logout.js"
import NotFound from "./pages/NotFound.js"

export default function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem("email")
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  let loggedIn = (user.email !== null) ? 
    <Route exact path="/register" component={Courses}/>
  : 
    <Route exact path="/register" component={Register}/>

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavbar/>
        <Container>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/courses" component={Courses}/>
            {loggedIn}
            <Route exact path="/login" component={Login}/>
            <Route exact path="/logout" component={Logout}/>
            <Route component={NotFound}/>
          </Switch>
        </Container>
      </BrowserRouter>
    </UserProvider>
  )
}